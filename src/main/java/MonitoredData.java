import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


/**
 * Created by Rosca on 14.05.2017.
 */
public class MonitoredData {
    private Date startTime;
    private Date endTime;
    private String activity;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    private DateFormat dateFormatWithoutHour = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);


    public MonitoredData(Date startTime, Date endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public Date getStartTime() {
        return startTime;
    }


    public Date getEndTime() {
        return endTime;
    }

    @Override
    public String toString() {
        String startTime = dateFormat.format(getStartTime());
        String endTime = dateFormat.format(getEndTime());
        return startTime + "  " + endTime + "  " + getActivity();
    }

    public String getActivity() {
        return activity;
    }


    public String getDataWithoutHour(){
        return dateFormatWithoutHour.format(startTime);
    }

    public long getActivityDuration(){
        long diffInMiliSec = endTime.getTime() - startTime.getTime();
        return diffInMiliSec;
    }
}
