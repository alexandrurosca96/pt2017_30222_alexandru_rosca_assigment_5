/**
 * Created by Rosca on 14.05.2017.
 */
public class Main {
    public static void main(String args[]){
        ProcessingData processingData = ProcessingData.getInstance();
        processingData.getDataFromFile();

        System.out.println("\nCount distinct days:");
        processingData.countDistinctDays();

        System.out.println("\nCount distinct Activities:");
        processingData.countDistinctActivities();

        System.out.println("\nCompute duration for each day bigger than 10h:");
        processingData.computeDurationForEachActivity();

        System.out.println("\nCount distinct activities per day:");
        processingData.countDistinctActivitiesPerDays();

        System.out.println("\nActivities with 90% of time shorter than 5 min:");
        processingData.activitiesWithShortDuration();
    }
}
