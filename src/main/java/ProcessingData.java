import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

/**
 * Created by Rosca on 14.05.2017.
 */
public class ProcessingData {
    private static ProcessingData ourInstance = new ProcessingData();

    public static ProcessingData getInstance() {
        return ourInstance;
    }

    private ProcessingData() {
    }
    private static final long tenHours = 1000* 60 * 60* 10;
    private static final long fiveMin = 1000* 60 * 5;

    List<MonitoredData> list = new ArrayList<MonitoredData>();
    DateFormat dateFormatWithoutHour = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    DateFormat dateFormatForHour = new SimpleDateFormat("dd HH:mm:ss", Locale.ENGLISH);


    public void countDistinctActivities(){
        Map<String, Long> resultSet;
        resultSet = list.stream().collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));

        resultSet.forEach((v,k) -> {System.out.println("Activity : " + v  + "  count : " + k);});
    }

    public void countDistinctActivitiesPerDays(){
        Map<String, Map<String, Long>> resultSet;
        resultSet = list.stream().collect(Collectors.groupingBy(MonitoredData::getDataWithoutHour,
                Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));

        resultSet.forEach((v, k) -> {System.out.println("Day : " + v);
                                    k.forEach((v1, k1)->{
                                        System.out.println("Activity : " + v1 + " count : " + k1);
                                    });
                        });

    }

    public void computeDurationForEachActivity(){
         Map<String, Date> result =  list.stream().collect(Collectors.groupingBy(MonitoredData::getActivity,
                 Collectors.summingLong(MonitoredData::getActivityDuration)))
                .entrySet().stream().filter(data -> data.getValue() > tenHours)
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> convertDate(entry.getValue())));

         result.forEach((v,k) -> {System.out.println("Activities : " + v  + "  Total duration : " + dateFormatForHour.format(k));});
    }

    public void activitiesWithShortDuration(){
        List<String> result = list.stream()
                .filter(data ->  getNoOfActivitiesLessThanFiveMin(data.getActivity()) / getNoOfActivities(data.getActivity()) > 0.9)
                .map(data -> data.getActivity()).distinct().collect(Collectors.toList());

        System.out.println(result);
    }

    public void countDistinctDays(){
        long count = list.stream()
                .map(data-> dateFormatWithoutHour.format(data.getStartTime()))
                .distinct().count();
        System.out.println("There are " + count  + " distinct days");
    }

    public long getNoOfActivities(String string){
        long count = list.stream().filter(data -> data.getActivity().equals(string))
                                  .count();
        return count;
    }

    public long getNoOfActivitiesLessThanFiveMin(String string){
        long count = list.stream().filter(data -> data.getActivity().equals(string))
                                  .filter(data -> data.getActivityDuration() < fiveMin)
                                  .count();
        return count;
    }

    public void getDataFromFile(){
        try {
            BufferedReader in = new BufferedReader(new FileReader("Activities.txt"));
            String str;
            while ((str = in.readLine()) != null)
                processLine(str);
            in.close();
        } catch (IOException e) {
        }
    }

    private void processLine(String str) {
        String []splitParts;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date dateStart, dateEnd;
        String activity;

        splitParts = str.split("\\t\\t");
        try {
            dateStart = dateFormat.parse(splitParts[0]);
            dateEnd = dateFormat.parse(splitParts[1]);
            activity = splitParts[2];
            list.add(new MonitoredData(dateStart, dateEnd, activity));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        String returnString = "";
        for(MonitoredData monitoredData: list){
            returnString += monitoredData.toString() + "\n";
        }
        return returnString;
    }

    public Date convertDate(long miliseconds){
        Date date = new Date(miliseconds);
        try {
            String string = dateFormatForHour.format(date);
            return dateFormatForHour.parse(string);
        } catch (ParseException e) {
            System.out.println("Cant parse");
        }
        return null;
    }
}
